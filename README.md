### English Homework Test

1. 
   - [X] a) Is Amy from?
   - [ ] b) Amy is from?
   - [ ] c) Does Amy from?
---
2. 
   - [ ] a) The children are doing?
   - [X] b) Are the children doing?
   - [ ] c) Doing the children?
---
3.  
   - [X] a) Doesn't play
   - [ ] b) Don't play
   - [ ] c) Not play
   - [ ] d) No play
---
4. 
   - [ ] a) Are you getting up?
   - [X] b) Do you usually get up?
   - [ ] c) Does you get up?
---
5. 
   - [ ] a) You had
   - [ ] b) Did you had
   - [ ] c) Are you have
   - [X] d) Did you have
---
6.  
   - [X] a) Does not arrive yet
   - [ ] b) Has not arrived yet
   - [ ] c) Has not arrive yet
---
7.  
   - [ ] a) Since three years
   - [X] b) Three years ago
---
8. 
   - [X] a) I left
   - [ ] b) I’ve left
---
9.  ---
   - [ ] a) I watch it.
   - [ ] b) I like watching it.
   - [X] c) I’m going to watch it.
---
10. 
    - [ ] a) I might go
    - [X] b) I might to go
    - [ ] c) I might going
    - [ ] d) I might be go
---
11. 
    - [ ] a) Mustn’t
    - [ ] b) Needn’t to
    - [X] c) Don’t need to
    - [ ] d) Haven’t to
---
12. 
    - [ ] a) Should
    - [ ] b) Have to
    - [ ] c) Must
    - [X] d) Had to
---
13. 
    - [ ] a) You like to
    - [ ] b) Do you like to
    - [X] c) Would you like to
---
14. 
    - [ ] a) It isn’t
    - [ ] b) There aren’t
    - [X] c) There isn’t
---
15. 
    - [X] a) I do
    - [ ] b) I am
    - [ ] c) I have
---
16. 
    - [ ] a) Do you
    - [ ] b) Will you
    - [ ] c) You don’t
    - [X] d) Won’t you
---
17. ---
    - [X] a) So haven’t I.
    - [ ] b) Neither have I.
    - [ ] c) Neither do I.
---
18. 
    - [X] a) Haven’t been
    - [ ] b) Didn’t go
    - [ ] c) Didn’t been

 
---
19. 
    - [ ] a) Diana isn’t
    - [X] b) Isn’t Diana
    - [ ] c) Diana not
    - [ ] d) Didn’t Diana
---
20. 
    - [ ] a) Which
    - [ ] b) Who
    - [X] c) What
---
21.  
    - [ ] a) Say
    - [X] b) Told
    - [ ] c) Said
---
22. 
    - [ ] a) To go
    - [X] b) Going
    - [ ] c) Go
---
23. 
    - [ ] a) Enjoy listen
    - [ ] b) Enjoy listening
    - [X] c) Enjoy to listen
    - [ ] d) Enjoying listen
---
24. 
    - [ ] a) Buying
    - [ ] b) Buy
    - [X] c) To buy
    - [ ] d) For buying
---
25. 
    - [X] a) Made
    - [ ] b) Did
---
26. 
    - [ ] a) Him
    - [ ] b) It
    - [X] c) Them
    - [ ] d) They
---
27. 
    - [ ] a) They
    - [X] b) Their
    - [ ] c) Them
    - [ ] d) Theirs
---
28.  
    - [ ] a) Themselves
    - [ ] b) Us
    - [ ] c) Ourselves
    - [X] d) Each other
---
29. 
    - [ ] a) Parents
    - [ ] b) Parent's
    - [X] c) Parents'
---
30. 
    - [X] a) A good weather
    - [ ] b) Good weathers
    - [ ] c) Good weather
---
31. 
    - [X] a) The best
    - [ ] b) Best
    - [ ] c) A best
---
32. 
    - [X] a) A new one
    - [ ] b) Some new ones
    - [ ] c) The new ones
    - [ ] d) New one
---
33. 
    - [ ] a) Most
    - [X] b) Most of
    - [ ] c) Every
---
34. 
    - [ ] a) Few
    - [ ] b) A little
    - [ ] c) Little
    - [X] d) A few
---
35. 
    - [ ] a) Good or bad
    - [X] b) Well or badly
    - [ ] c) Well or bad
    - [ ] d) Good or badly
---
36. 
    - [ ] a) Further
    - [ ] b) More far
    - [X] c) Far
---
37.  
    - [X] a) Older than me
    - [ ] b) More old as me
    - [ ] c) Old than me
    - [ ] d) More old than me
---
38. 
    - [ ] a) Not as expensive
    - [X] b) Not as expensive than
    - [ ] c) Not as expensive as
---
39. 
    - [ ] a) The more expensive
    - [ ] b) Most expensive
    - [X] c) The most expensive
---
40. 
    - [ ] a) Doesn’t have money enough
    - [ ] b) Isn’t enough money
    - [X] c) Doesn’t have enough money

---
41. 
    - [X] a) French very well
    - [ ] b) Very well French
---
42. 
    - [ ] a) Go usually out
    - [X] b) Usually go out
    - [ ] c) Go out usually
---
43. 
    - [ ] a) Not yet
    - [ ] b) Already
    - [X] c) Yet
    - [ ] d) Still
---
44. 
    - [X] a) Give Mark
    - [ ] b) Give to Mark
    - [ ] c) Give it to Mark
---
45. 
    - [ ] a) Until
    - [X] b) Since
    - [ ] c) To
    - [ ] d) For
---
46. 
    - [ ] a) To
    - [ ] b) By
    - [X] c) At
    - [ ] d) On
---
47. 
    - [ ] a) Afraid with
    - [X] b) Afraid about
    - [ ] c) Afraid for
    - [ ] d) Afraid of
---
48. 
    - [X] a) Fell off his bike
    - [ ] b) Fell his bike off
    - [ ] c) Fell down his bike
---
49. 
    - [ ] a) But
    - [ ] b) So
    - [X] c) Because
    - [ ] d) Or
---
50. 
    - [ ] a) Would miss
    - [ ] b) Are missing
    - [ ] c) Miss
    - [X] d) Will miss
